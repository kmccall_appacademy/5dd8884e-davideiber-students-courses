class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    [@first_name, @last_name].join(' ')
  end

  def enroll(course_obj)
    conflicts = @courses.select { |course| course_obj.conflicts_with?(course) }
    if conflicts.empty?
      @courses << course_obj
      @courses.last.students << self
    else
      raise "There is a conflict."
    end
  end

  def courses
    @courses
  end

  def course_names
    names = []
    @courses.each do |course|
      names << course.name
    end
  end

  def course_load
    result = Hash.new{ 0 }
    @courses.each do |course|
      result[course.department] += course.credits
    end
    result
  end
end
